// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Test{
    address public addressName;
    int public number;

    constructor(){
        addressName = msg.sender;
        number = 0;
    }

    function increment() external {
        number = number + 1;
    }

    function decrement() external {
        number = number - 1;
    }

}