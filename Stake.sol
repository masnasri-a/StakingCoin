// SPDX-License-Identifier: MIT
pragma solidity ^0.8.6;

import "/home/nasri/Documents/StackingToken/StakingCoin/node_modules/@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "/home/nasri/Documents/StackingToken/StakingCoin/node_modules/@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "/home/nasri/Documents/StackingToken/StakingCoin/node_modules/@openzeppelin/contracts/access/Ownable.sol";
import "/home/nasri/Documents/StackingToken/StakingCoin/node_modules/@openzeppelin/contracts/utils/math/SafeMath.sol";
import "/home/nasri/Documents/StackingToken/StakingCoin/node_modules/@openzeppelin/contracts/utils/Context.sol";

contract Stake is Context, Ownable{

    using SafeMath for uint;

    uint BIGNUMBER = 10**18;
    uint DECIMAL = 10**3;

    event claimed(address _address, uint amount);

    struct dataStaking{
        uint amount;
        bool requested;
        uint endStakeDate;
        uint createdAt;
    }

    mapping (address => bool) public allowedTokens;

    mapping (address => mapping ( address => dataStaking )) public MapOfStake;
    mapping (address => mapping (address => uint )) userRewardPerStake;
    mapping (address => uint ) tokenReward;
    mapping (address => uint) totalTokenStaked;

    mapping (address => address) mediator;

    modifier isAllowedToken(address _address){
        require(allowedTokens[_address]);
        _;
    }

    modifier isMediator(address _address){
        require(mediator[_address] == _msgSender());
        _;
    }

    address public StakeTokenAddr;
    
    constructor(address _address) public{
        StakeTokenAddr= _address;
    }

    function addToken( address _address) onlyOwner external {
        allowedTokens[_address] = true;
    }
    
    function delToken(address _address) onlyOwner external {
        allowedTokens[_address] = false;
    }

    function stake(address _address, uint _amount) isAllowedToken(_address) external returns (bool){
        require(_amount != 0);

        if ( MapOfStake[_address][_msgSender()].amount != 0 ){
            claimReward(_address);
            MapOfStake[_address][_msgSender()].amount = MapOfStake[_address][_msgSender()].amount.add( _amount);
        }else{
            MapOfStake[_address][_msgSender()].amount = _amount;
            userRewardPerStake[_address][_msgSender()] = tokenReward[_address];
        }
        totalTokenStaked[_address] = totalTokenStaked[_address].add(_amount);
        return true;
    }
    
    function claimReward(address _address) isAllowedToken(_address) public{
        uint accumStakeAmount = MapOfStake[_address][_msgSender()].amount;
        uint amountOwedPerToken = tokenReward[_address].sub(userRewardPerStake[_address][_msgSender()]);
        uint claimableAmount = accumStakeAmount.mul(amountOwedPerToken);
        claimableAmount = claimableAmount.mul(DECIMAL); 
        claimableAmount = claimableAmount.div(BIGNUMBER); 
        userRewardPerStake[_address][_msgSender()]=tokenReward[_address];
        emit claimed(_address, claimableAmount);
    }


    function distribute(uint _reward,address _address) isAllowedToken(_address) external returns (bool){
        require(totalTokenStaked[_address] != 0);
        uint reward = _reward.mul(BIGNUMBER); //simulate floating point operations
        uint rewardAddedPerToken = reward/totalTokenStaked[_address];
        tokenReward[_address] = tokenReward[_address].add(rewardAddedPerToken);
        return true;
    }
    
    function initWithdraw(address _address, uint _createdAt, uint _endStakeDate) isAllowedToken(_address)  external returns (bool){
        require(MapOfStake[_address][_msgSender()].amount >0 );
        require(! MapOfStake[_address][_msgSender()].requested );
        MapOfStake[_address][_msgSender()].createdAt = _createdAt;
        MapOfStake[_address][_msgSender()].endStakeDate = _endStakeDate;
        return true;

    }
    
    function finalizeWithdraw(uint _amount, address _address) isAllowedToken(_address)  external returns(bool){
        require(MapOfStake[_address][_msgSender()].amount >0 );
        require(MapOfStake[_address][_msgSender()].requested );
        require(MapOfStake[_address][_msgSender()].endStakeDate > MapOfStake[_address][_msgSender()].createdAt );
        claimReward(_address);
        require(ERC20(_address).transfer(_msgSender(),_amount));
        totalTokenStaked[_address] = totalTokenStaked[_address].sub(_amount);
        MapOfStake[_address][_msgSender()].requested = false;
        return true;
    }
    
    function releaseStake(address _address, address[] memory _stakers, uint[] memory _amounts,address _dest) isMediator(_address) isAllowedToken(_address) external returns (bool){
        require(_stakers.length == _amounts.length);
        for (uint i =0; i< _stakers.length; i++){
            require(IERC20(_address).transfer(_dest,_amounts[i]));
            MapOfStake[_address][_stakers[i]].amount -= _amounts[i];
        }
        return true;
    }

}